//Лабораторная работа №10 "Программа-СУБД о студентах"
//Выполнил студент группы ИСТб-81 Дёгтев И.В.

#include <stdio.h>
#include <string.h>
#include <windows.h>

#define WAY_TO_FILE_BD "F:\\I131\\C\\Student\\BD\\Student.bd"
#define WAY_TO_NEW_FILE_BD "F:\\I131\\C\\Student\\BD\\New.buf"

#define MAX_VALUE_OF_STRING 25

struct student
{
    char name[MAX_VALUE_OF_STRING]; //Имя
    char sureName[MAX_VALUE_OF_STRING]; //Фамилия
    char secondName[MAX_VALUE_OF_STRING]; //Отчество
    unsigned int yearOfBorn; //Год рождения
    char group[MAX_VALUE_OF_STRING]; //Номер группы
    unsigned int numberOfKommun;//Номер общежития или 0, если студент не проживает в общежитии
    float stip; //Стипендия
    float ball; //Средний балл за проследнюю сессию
}Student;

//Функция добавления в БД записи о студенте.
//Аргумент: указатель на файл БД.
//Возвращает код ошибки или 0
int Add(FILE* fp)
{
    fp = fopen(WAY_TO_FILE_BD, "a");
    if (fp == NULL)
    {
        printf("Упс! Нет файла БД или прав!\7\n\n");

        return -2;
    }

    printf("Введите фамилию студента: ");
    scanf("%s", Student.sureName);
    printf("Введите имя студента: ");
    scanf("%s", Student.name);
    printf("Введите отчество студента: ");
    scanf("%s", Student.secondName);
    printf("Введите год рождения: ");
    scanf("%u", &Student.yearOfBorn);
    printf("Введите номер общежития или 0, если студент не проживает в общежитии: ");
    scanf("%u", &Student.numberOfKommun);
    printf("Введите номер группы: ");
    scanf("%s", Student.group);
    printf("Введите размер начисляемой стипендии: ");
    scanf("%f", &Student.stip);
    printf("Введите средний балл за последнюю сессию: ");
    scanf("%f", &Student.ball);

    fwrite(&Student, sizeof(Student), 1, fp);

    fclose(fp);

    printf("\nЗапись прошла успешно!\n\n");

    return 0;
}

//Функция печатания студента
void printStudent()
{
    int i;
    int n = strlen("      Дёгтев     Илья         Вячеславович|         2000| ИСТб-81|              0|         2500.00|                            4.50");

    printf("\n|%10s %10s %20s|%13u|%8s|%15u|%16.2f|%32.2f|\n", Student.sureName, Student.name, Student.secondName, Student.yearOfBorn, Student.group, Student.numberOfKommun, Student.stip, Student.ball);
    printf("+");
    for (i = 0; i < n; i++)
        printf("-");
    printf("+");
}

//функция печатания таблицы
void printTable()
{
    int i;
    int n = strlen("     Дёгтев     Илья          Вячеславович|         2000| ИСТб-81|              0|         2500.00|                            4.50");

    printf("+");
    for (i = 0; i < n; i++)
        printf("-");
    printf("+");
    printf("\n");
    printf("|ФИО");
    for (i = 0; i < strlen("Дёгтев      Илья              Вячеславович")-3; i++)
        printf(" ");
    printf("|Год рождения");
    for (i = 0; i < strlen("         2000")-12; i++)
        printf(" ");
    printf("|Группа");
    for (i = 0; i < strlen(" ИСТб-81")-6; i++)
        printf(" ");
    printf("|Номер общежития");
    for (i = 0; i < strlen("              0")-15; i++)
        printf(" ");
    printf("|Размер стипендии");
    for (i = 0; i < strlen("         2500.00")-16; i++)
        printf(" ");
    printf("|Средний балл за последнюю сессию|\n");
    printf("+");
    for (i = 0; i < n; i++)
        printf("-");
    printf("+");
}

//Функция удаления из БД записи о студенте.
//Аргумент: указатель на файл БД.
//Возвращает код ошибки или 0
int Del(FILE* fp)
{
    char sureName[MAX_VALUE_OF_STRING];
    char name[MAX_VALUE_OF_STRING];
    char secondName[MAX_VALUE_OF_STRING];
    unsigned int yearOfBorn;
    unsigned char found = 0;
    FILE* fpnew;//Новый промежуточный файл
    int ren_err;
    int rem_err;

    fp = fopen(WAY_TO_FILE_BD, "r");
    if (fp == NULL)
    {
        printf("Упс! Нет файла БД или прав!\7\n\n");

        return -4;
    }

    printf("Введите фамилию: ");
    scanf("%s", sureName);
    printf("Введите имя: ");
    scanf("%s", name);
    printf("Введите отчество: ");
    scanf("%s", secondName);
    printf("Введите год рождения: ");
    scanf("%u", &yearOfBorn);

    fpnew = fopen(WAY_TO_NEW_FILE_BD, "w");
    if (fpnew == NULL)
    {
        printf("\nНе получилось создать буферный файл!\7\n\n");

        return -5;
    }

    while(!feof(fp))
    {
        fread(&Student, sizeof(Student), 1, fp);
        if(!strcmp(Student.sureName, sureName) && !strcmp(Student.name, name) && !strcmp(Student.secondName, secondName) && (Student.yearOfBorn == yearOfBorn))
        {
            found = 1;
        }
        else
        {
            if (!feof(fp))
            {
             fwrite(&Student, sizeof(Student), 1, fpnew);
            }
        }
    }

    fclose(fp);
    fclose(fpnew);

    if(!found)
    {
        printf("\nНе удалили, ибо не нашли студента.\7\n\n");

        return 2;
    }

    rem_err = remove(WAY_TO_FILE_BD);
    if (rem_err == -1)
    {
        printf("Ошибка удаления файла БД!\7\n\n");

        return 6;
    }
    ren_err = rename(WAY_TO_NEW_FILE_BD, WAY_TO_FILE_BD);

    if (ren_err == -1)
    {
        printf("Ошибка переименования файла БД!\7\n\n");

        return 7;
    }

    printf("\nУдаление прошло успешно!\n\n");

    return 0;
}

//Функция поиска по фамилии
//Аргумент: указатель на файл БД.
//Возвращает код ошибки или 0
int SeeSureName(FILE* fp)
{
    char sureName[MAX_VALUE_OF_STRING];//Буферная фамилия для сравнения.
    unsigned char found = 0;

    printf("Введите фамилию: ");
    scanf("%s", sureName);

    fp = fopen(WAY_TO_FILE_BD, "r");
    if (fp == NULL)
    {
        printf("Упс! Нет файла БД или прав!\7\n\n");

        return -2;
    }
    while(!feof(fp))
    {
        fread(&Student, sizeof(Student), 1, fp);
        if (!strcmp(sureName, Student.sureName))
        {
            found += 1;
            if (found == 1)
            {
                printTable();
            }
            if(!feof(fp))
                printStudent();
        }
    }

    fclose(fp);

    if (found < 1)
    {
        printf("\nУпс! Нет такого студента!\7\n\n");

        return 3;
    }

    printf("\n\n");

    return 0;
}

//Функция поиска по диапазону среднего балла
//Аргумент: указатель на файл БД.
//Возвращает код ошибки или 0
int SeeBall(FILE* fp)
{
    float ballMax, ballMin;
    unsigned char found = 0;

    printf("Введите минимум среднего балла: ");
    scanf("%f", &ballMin);
    printf("Введите максимум среднего балла: ");
    scanf("%f", &ballMax);
    if (ballMax < ballMin)
    {
        printf("Максимум не может быть меньше минимума!\7\n\n");

        return 8;
    }

    fp = fopen(WAY_TO_FILE_BD, "r");
    if (fp == NULL)
    {
        printf("Упс! Нет файла БД или прав!\7\n\n");

        return -6;
    }
    while(!feof(fp))
    {
        fread(&Student, sizeof(Student), 1, fp);
        if (Student.ball <= ballMax && Student.ball >= ballMin)
        {
            found += 1;
            if (found == 1)
            {
                printTable();
            }
            if(!feof(fp))
                printStudent();
        }
    }

    fclose(fp);

    if (found < 1)
    {
        printf("\nУпс! Нет такого студента!\7\n\n");

        return 7;
    }

    printf("\n\n");

    return 0;
}

//Функция поиска по номеру группы
//Аргумент: указатель на файл БД.
//Возвращает код ошибки или 0
int SeeNumberGroup(FILE* fp)
{
    char group[MAX_VALUE_OF_STRING];
    unsigned char found = 0;

    printf("Введите номер группы: ");
    scanf("%s", group);

    fp = fopen(WAY_TO_FILE_BD, "r");
    if (fp == NULL)
    {
        printf("Упс! Нет файла БД или прав!\7\n\n");

        return -8;
    }
    while(!feof(fp))
    {
        fread(&Student, sizeof(Student), 1, fp);
        if (!strcmp(group, Student.group))
        {
            found += 1;
            if (found == 1)
            {
                    printTable();
            }
            if(!feof(fp))
                printStudent();
        }
    }

    fclose(fp);

    if (found < 1)
    {
        printf("\nУпс! Нет такого студента!\7\n\n");

        return 8;
    }

    printf("\n\n");

    return 0;
}

//Функция поиска по диапазону годов рождения
//Аргумент: указатель на файл БД.
//Возвращает код ошибки или 0
int SeeYearOfBorn(FILE* fp)
{
    unsigned int yearMax, yearMin;
    unsigned char found = 0;

    printf("Введите минимум года рождения: ");
    scanf("%u", &yearMin);
    printf("Введите максимум года рождения: ");
    scanf("%u", &yearMax);

    if (yearMax < yearMin)
    {
        printf("Максимум не может быть меньше минимума!\7\n\n");

        return 9;
    }

    fp = fopen(WAY_TO_FILE_BD, "r");
    if (fp == NULL)
    {
        printf("Упс! Нет файла БД или прав!\7\n\n");

        return -9;
    }
    while(!feof(fp))
    {
        fread(&Student, sizeof(Student), 1, fp);
        if (Student.yearOfBorn >= yearMin && Student.yearOfBorn <= yearMax)
        {
            found += 1;
            if (found == 1)
            {
                printTable();
            }
                if(!feof(fp))
                    printStudent();
        }
    }

    fclose(fp);

    if (found < 1)
    {
        printf("\nУпс! Нет такого студента!\7\n\n");

        return 8;
    }

    printf("\n\n");

    return 0;
}

//Функция показа всех студентов
//Аргумент: указатель на файл БД.
//Возвращает код ошибки или 0
int SeeAll(FILE* fp)
{
    unsigned char found = 0;

    fp = fopen(WAY_TO_FILE_BD, "r");
    if (fp == NULL)
    {
        printf("Упс! Нет файла БД или прав!\7\n\n");

        return -10;
    }

    printf("Все студенты: \n");
    while(!feof(fp))
    {
        if (Student.name != "")//Если имя студента не содержит ни одного символа, значит, студентов в базе нет в принципе, потому что хоть сколько-то мы прочитали.
        {
            fread(&Student, sizeof(Student), 1, fp);
            found += 1;
            if (found == 1)
            {
                printTable();
            }
            if(!feof(fp))
                printStudent();
        }
    }

    fclose(fp);

    if (found < 1)
    {
        printf("Упс! А у нас нет студентов! Попробуйте внести кого-нибудь в базу.\n");

        return 10;
    }

    printf("\n\n");

    return 0;
}

//Функция показа студентов в общежитии или вне его
//Аргумент: указатель на файл БД.
//Возвращает код ошибки или 0
int SeeKommun(FILE* fp)
{
    unsigned char found;
    unsigned int numberOfKommun;

    printf("Введите номер общежития, если хотите увидеть всех студентов, проживающих в этом общежитии, и 0, если хотите увидеть всех студентов без общежития: ");
    scanf("%u", &numberOfKommun);

    fp = fopen(WAY_TO_FILE_BD, "r");
    if (fp == NULL)
    {
        printf("Упс! Нет файла БД или прав!\7\n\n");

        return -11;
    }

    while(!feof(fp))
    {
        fread(&Student, sizeof(Student), 1, fp);
        if (numberOfKommun == Student.numberOfKommun)
        {
            found += 1;
            if (found == 1)
            {
                printTable();
            }
            if(!feof(fp))
                printStudent();
        }
    }

    fclose(fp);

    if (found < 1)
    {
        {
            printf("\nУпс! А у нас нет студентов!\7\n\n");

            return 11;
        }
    }

    printf("\n\n");

    return 0;
}



int main()
{
    FILE* filePointer;
    int number = -1; //Чтобы switch не выбирал сразу
    int error_number = 0; //Код ошибки, изначально всё хорошо

    SetConsoleCP(65001);
    SetConsoleOutputCP(65001);

    printf("Вас приветствует СУБД о студентах!\n\n");

    //Меню прямо тут.
    while(number != 0)
    {
        printf("Выберете тип операции:\n");
        printf("0: Выход из СУБД.\n");
        printf("1: Добавление студента в базу.\n");
        printf("2: Удаление студента из базы.\n");
        printf("3: Найти студента по фамилии.\n");
        printf("4: Найти студентов по диапазону среднего балла.\n");
        printf("5: Найти всех студентов из группы.\n");
        printf("6: Найти студентов по диапазону годов рождения.\n");
        printf("7: Показать всех студентов.\n");
        printf("8: Показать всех студентов, проживающих/не проживающих в общежитии.\n");

        printf("\nИтак, делаем операцию номер: ");
        scanf("%d", &number);
        printf("\n");

        switch(number)
        {
           case 0: printf("Выход. До свидания!\n"); system("pause"); break;
           case 1: error_number = Add(filePointer); break;
           case 2: error_number = Del(filePointer); break;
           case 3: error_number = SeeSureName(filePointer); break;
           case 4: error_number = SeeBall(filePointer); break;
           case 5: error_number = SeeNumberGroup(filePointer); break;
           case 6: error_number = SeeYearOfBorn(filePointer); break;
           case 7: error_number = SeeAll(filePointer); break;
           case 8: error_number = SeeKommun(filePointer); break;

           default: printf("Некорректное значение!\n\n"); error_number = 1;
        }
    }

    return error_number;
}
